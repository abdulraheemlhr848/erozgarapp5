import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TextInput,
} from "react-native";
import React from "react";
import { CustomInput } from "../components/CustomInput";

export default function Details({ route }) {
  const data = route.params;
  console.log(data.users);
  return (
    <View>
      <CustomInput icon={"eye"} placeholderText={"details"} />
      <Image style={styles.img} source={{ uri: data.selectedImage }} />
      <FlatList
        data={data.users}
        renderItem={({ item }) => (
          <View style={{ padding: 10, margin: 20 }}>
            <Text>{item.name}</Text>
            <Text>{item.rollNumer}</Text>
            <Text>{item.description}</Text>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    width: "100%",
    height: "59%",
    resizeMode: "center",
  },
});
