const makeBlobFromUri = async (uri) => {
  const img = await fetch(uri);
  const blob = await img.blob();
  return blob;
};

export { makeBlobFromUri };
